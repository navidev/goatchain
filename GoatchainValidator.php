<?php

declare(strict_types=1);

require_once('Goat.php');

class GoatchainValidator
{
    public function isValidGoatchain(array $goatchain): bool
    {
        if (
            $goatchain === null
            || empty($goatchain)
            || $goatchain[0]->getIndex() !== 0
            || $goatchain[0]->getPreviousGoatHash() !== null
        ) {
            return false;
        }

        foreach ($goatchain as $key => $goat) {
            if ($key != 0 && strcmp($goatchain[$key - 1]->getHash(), $goat->getPreviousGoatHash()) !== 0) {
                return false;
            }
        }

        return true;
    }

    public function isValidGoat(Goat $goat, array $goatchain): bool
    {
        $lastGoat = end($goatchain);

        if (
            $goat === null
            || $goat->getIndex() !== $lastGoat->getIndex() + 1
            || strcmp($goat->getPreviousGoatHash(), $lastGoat->getHash()) !== 0
            || $goat->getTimestamp() < $lastGoat->getTimestamp()
            || empty($goat->getData())
        ) {
            return false;
        }


        return true;
    }
}
