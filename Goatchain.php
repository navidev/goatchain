<?php

declare(strict_types=1);

require_once('Goat.php');
require_once('GoatchainValidator.php');

/**
 * * Singleton class as a single instance of the Goatchain is needed
 */
class Goatchain
{
    private static $instance = null;
    private $difficulty;
    private $goatchain;
    private $validator;

    private function __construct(int $difficulty = null)
    {
        $this->difficulty = $difficulty ?? 1;
        $this->goatchain[] = $this->initGoatnesis();
        $this->validator = new GoatchainValidator();
    }

    public static function getInstance(int $difficulty = null)
    {
        if (is_null(self::$instance)) {
            self::$instance = new Goatchain($difficulty);
        }

        return self::$instance;
    }

    public function pushGoat(Goat $goat)
    {
        array_push($this->goatchain, $goat);
    }

    public function prepareGoat(array $data): Goat
    {
        if (!$this->validator->isValidGoatchain($this->goatchain)) {
            throw new Exception('in: "' . __FUNCTION__  . '" : Goatchain is not valid! Stopping Goat prep.' . PHP_EOL);
        } elseif (empty($data)) {
            throw new Exception('ERROR in "' . __FUNCTION__ . '" : Parameter array should not be empty.' . PHP_EOL);
        }

        $lastGoat = end($this->goatchain);
        $goat = new Goat($lastGoat->getIndex() + 1, $lastGoat->getHash(), $data, $this->difficulty);
        
        $goat->mineGoat();

        if (!$this->validator->isValidGoat($goat, $this->goatchain)) {
            throw new Exception('in: "' . __FUNCTION__  . '" : Goat is not valid! Stopping Goat prep.' . PHP_EOL);
        }

        return $goat;
    }

    private function initGoatnesis(): Goat
    {
        $goatnesis = new Goat(0, null, ['GOATNESIS'], $this->difficulty);

        return $goatnesis->mineGoat();
    }

    // * --- Getters and setters ----------------------------------

    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    public function getGoatchainAsArray(): array
    {
        $result = array();

        foreach ($this->goatchain as $object) {
            array_push($result,  (array)$object);
        }

        return $result;
    }
}
