<?php

declare(strict_types=1);

class Goat
{
    private $index;
    private $timestamp;
    private $hash;
    private $previousGoatHash;
    private $data;
    private $runningDifficulty;

    public function __construct(int $index, ?string $previousGoatHash, array $data, int $runningDifficulty)
    {
        $this->index = $index;
        $this->timestamp = round(microtime(true) * 1000);
        $this->hash = null;
        $this->previousGoatHash = $previousGoatHash;
        $this->data = $data;
        $this->runningDifficulty = $runningDifficulty;
    }
    
    public function mineGoat(): ?self
    {
        if (!$hash = $this->genGoatHash($this)) {
            return null;
        }

        $hashZeros = substr($hash, 0, $this->runningDifficulty);

        while (strcmp($hashZeros, $this->difficultyToZeros($this->runningDifficulty)) !== 0) {
            $hash = $this->genGoatHash($this);
            $hashZeros = substr($hash, 0, $this->runningDifficulty);
        }

        $this->hash = $hash;
        $this->timestamp = round(microtime(true) * 1000);

        return $this;
    }

    private function genGoatHash(Goat $goat): ?string
    {
        $str = null;

        foreach ($goat->getData() as $data) {
            $str .= $data;
        }

        return hash('sha256', $str . round(microtime(true) * 1000));
    }

    private function difficultyToZeros(int $difficulty): string
    {
        return sprintf("%'.0{$difficulty}d", 0);
    }


    // * --- Getters and setters ----------------------------------

    public function getIndex(): int
    {
        return $this->index;
    }

    public function getTimestamp(): int
    {
        return (int) $this->timestamp;
    }

    public function getTries(): int
    {
        return $this->tries;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function getPreviousGoatHash(): ?string
    {
        return $this->previousGoatHash;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
